#coding=utf-8
import os
try:
    from flaskext.sqlalchemy import SQLAlchemy
except:
    from flask_sqlalchemy import SQLAlchemy

__ALL__ = ['db', 'DBSession', 'DeclarativeBase', 'metadata']

db = SQLAlchemy()
DBSession = db.session
DeclarativeBase = db.Model
metadata = db.metadata
'''
engine = create_engine(DB['url'], encoding=DB['charset'], echo=DB['echo'])
DBSession = scoped_session(sessionmaker(autocommit=True, autoflush=True, bind=engine))
DeclarativeBase = declarative_base()
metadata = DeclarativeBase.metadata
conn = engine.connect()
'''

os.environ["NLS_LANG"] = "american_america.al32utf8"
