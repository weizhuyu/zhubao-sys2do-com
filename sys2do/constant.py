# -*- coding: utf-8 -*-

SYSTEM_DATE_FORMAT = "%Y-%m-%d"
SYSTEM_TIME_FORMAT = "%H:%M"
SYSTEM_DATETIME_FORMAT = "%Y-%m-%d %H:%M"

TAB_FLAG = 'tab-highlight'
TAB_HOME = 'HOME'
TAB_BASIC = 'BASIC'
TAB_REPORT = 'RPT'
TAB_SETTING = 'SETTING'

MESSAGE_INFO = "INFO"
MESSAGE_ERROR = "ERROR"

IVTLT_VIRTUAL_INTRAVEL = 'IN_TRAVEL'
IVTLT_VIRTUAL_PURCHASE = 'PURCHASE'
IVTLT_VIRTUAL_SOLDOUT = 'SOLD_OUT'
IVTLT_VIRTUAL_WASTAGE = 'WASTAGE'
IVTLT_VIRTUAL_PROCESS = 'PROCESS'
IVTLT_VIRTUAL_LIST = [IVTLT_VIRTUAL_INTRAVEL, IVTLT_VIRTUAL_PURCHASE,
    IVTLT_VIRTUAL_SOLDOUT, IVTLT_VIRTUAL_WASTAGE, IVTLT_VIRTUAL_PROCESS
]

#===============================================================================
# button text
#===============================================================================
BTN_SEARCH = u'查询记录'
BTN_CANCEL = u'取消'
BTN_SUBMIT = u'确认提交'
BTN_UPDATE = u'编辑'
BTN_DELETE = u'删除'
BTN_ACCEPT = u'批准'
BTN_REJECT = u'拒绝'
BTN_ENTER = u'进入'
BTN_CLOSE = u'关闭'
BTN_EXPORT_RPT = u'导出报表'

# alert messages
MSG_NO_SUCH_ACTION = u'没有该操作！'
MSG_SAVE_SUCC = u'成功保存记录！'
MSG_UPDATE_SUCC = u'成功更新记录！'
MSG_DELETE_SUCC = u'成功删除记录！'
MSG_SERVER_ERROR = u'该服务暂时不可用，请稍后再试，或者联系系统管理员！'
MSG_FORBIDDEN_ACCESS = u'禁止访问！'
MSG_NO_ID_SUPPLIED = u'没有提供记录的ID！'
MSG_NOT_ENOUGH_PARAMS = u'没有提供足够的参数！'
MSG_NOT_ALL_PARAMS_OK = u'并不是全部参数都符合条件！'
MSG_RECORD_NOT_EXIST = u'该记录不存在！'
MSG_RECORD_ALREADY_EXIST = u'该记录已经存在！'
MSG_SAME_NAME_RECORD_EXIST = u'同名的记录已经存在！'
MSG_NO_FILE_UPLOADED = u'没有文件上传！'
MSG_INVALID_FILE_TO_UPLOAD = u'上传非法文件！'
MSG_USER_NOT_EXIST = u'该用户不存在！'
MSG_WRONG_PASSWORD = u'密码错误!'
MSG_WRONG_CAPTCHA = u'验证码错误!'
MSG_CONFIRM_DELETE = u'你确认删除该记录吗？'
MSG_ORDER_NOT_FIT_FOR_DELIVER = u'所选择的订单中有状态不能创建送货单的记录，请注意订单状态再重新创建！'
MSG_ATLEAST_ONE_ORDER_TO_CREATE_DELIVER = u'请选择至少一条记录以创建送货单！'
MSG_ATLEAST_ONE_ORDER_TO_EXPORT = u'请先选择订单然后再导出！'
MSG_LEAVE_WITHOUT_SAVING = u'确认不保存而离开这个页面吗？'
MSG_NO_PERMISSION = u'用户没有该操作的权限！'
MSG_EMAIL_NOT_EXIST = u'您输入的邮箱不存在'
MSG_SEND_EMAIL_GET_PASSWORD = u'已向您的邮箱发送了一封密码找回邮件，请您注意接收邮件'
MSG_RESET_PASSWORD_LINK_EXPIRY = u'该链接已失效，请重新找回密码'
MSG_RESET_PASSWORD_SUCCESS = u'已重置密码，请您用新密码登录系统'
MSG_FIELD_REQUIRED = u'请按提示输入空的数据！'
MSG_INVENTORY_NOT_SELECTED = u'请选择仓位！'
MSG_MORE_THAN_STOCK = u'该仓位现存数量不足，操作不能执行！'
MSG_NO_INVENTORY_QTY = u'仓位没有相应商品，或者库存不足，操作不能执行！'
MSG_NO_INVENTORY_PROVIDED = u'没有提供该操作的仓位！'
MSG_NO_RELATED_ITEM = u'没有相关联的商品个体，不能进行该操作。请先编辑相关信息！'
MSG_ITEM_QTY_NOT_MATCH = u'商品个体的数量与出入库数量不相符。请先编辑相关信息！'
MSG_PRODUCT_ITEM_NOT_MATCH = u'商品与个体的信息不匹配。请先编辑相关信息！'
MSG_ITEM_NO_IN_CORRECT_INVENTORY = u'商品个体不在与出入库单信息相符的仓位中。请先编辑相关信息！'
MSG_ITEM_ALREADY_OCCUPIED = u'商品个体已经被占用，不能进行该操作！'

#===============================================================================
# active value for all record
#===============================================================================
ACTIVE = 0
INACTIVE = 1


#===============================================================================
# log type
#===============================================================================
LOG_TYPE_CREATE = 'CREATE'
LOG_TYPE_UPDATE = 'UPDATE'
LOG_TYPE_DEL = 'DELETE'
LOG_TYPE_APPROVE = 'APPROVE'
LOG_TYPE_REJECT = 'REJECT'

#===============================================================================
# for product
#===============================================================================
PRODUCT_VALID = 0
PRODUCT_INVALID = 1
PRODUCT_SALEABLE = 0
PRODUCT_UNSALEABLE = 1

#===============================================================================
# for shop
#===============================================================================
SHOP_TYPE_MAIN = 0
SHOP_TYPE_BRANCH = 1
SHOP_SALE_TYPE_SELF = 0
SHOP_SALE_TYPE_JOIN = 1

#===============================================================================
# status for product
#===============================================================================
PDT_INVALID = 0
PDT_VALID = 1
PDT_SOLDOUT = 2
PDT_RETURN = 9


#===============================================================================
# status for item
#===============================================================================
ITEM_AVAILABLE = 0
ITEM_LOCKED = 1
ITEM_UNAVAILABLE = -1


#===============================================================================
# status for transfer
#===============================================================================
TNS_NEW = 0
TNS_SND_AP = 1
TNS_TRAVEL = 2
TNS_RCV = 3

#===============================================================================
# status for po
#===============================================================================
PO_NEW = 0
PO_APPROVE = 1
PO_DISAPPROVE = -1

#===============================================================================
# direction for inventory note
#===============================================================================
IVTNT_NEW = 0
IVTNT_APPROVE = 1
IVTNT_DISAPPROVE = -1
IVTNT_TO_WASTAGE = -2
IVTNT_IN = 'IN'
IVTNT_OUT = 'OUT'
IVTNT_INTERNAL = 'INTERNAL'

#===============================================================================
# direction for return goods note
#===============================================================================
RGN_NEW = 0
RGN_APPROVE = 1
RGN_REJECT = -1


#===============================================================================
# for FIN note
#===============================================================================
FIN_IN = 'IN'
FIN_OUT = 'OUT'
FIN_NEW = 0
FIN_APPROVE = 1
FIN_REJECT = -1


#===============================================================================
# for SO
#===============================================================================
SO_CLZ = 'SO'
SO_NEW = 0
SO_APPROVE = 1
SO_DISAPPROVE = -1
