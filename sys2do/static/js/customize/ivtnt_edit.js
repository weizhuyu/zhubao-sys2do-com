var srhResult = [];

function srhPdtObj(id){
    for(var i=0;i<srhResult.length;i++){
        var t = srhResult[i];
        if(t.pdivtid == id){ return t ;}
    }
    return null;
}


//*******************************************************
// for the IN
//*******************************************************

function ajaxSrhPdt(){
    var params = {
        'type' : 'A',
        'no' : $("#pdtno").val(),
        'name' : $("#pdtname").val(),
        'typeID' : $("#pdttypeID").val(),
        't' : nowstr()
    }
    
    $.getJSON('/ivtnt/ajaxSrhPdt',params,function (r){
        if(r.code!=0){
            alert(r.msg);
        }else{
            srhResult = r.data;
            var html = '';
            for(var i=0;i<r.data.length;i++){
                var t = r.data[i];
                html += '<tr><td><input type="checkbox" name="srhpdt" value="'+t.pdivtid+'"/></td>';
                html += '<td>'+t.no+'</td>';
                html += '<td>'+t.name+'</td>';
                html += '<td>'+t.type+'</td>';
                html += '</tr>';
            }            
            $("#srhbody").html(html);
        }
    })
}

function addPdt(obj){
    var html = '<tr>';
    html += '<td>'+obj.no+'</td>';

    html += '<td>'+obj.name+'<input type="hidden" name="product_'+obj.id+'_'+obj.ivtid+'" value="'+obj.id+'"/></td>';
    html += '<td>'+obj.type+'</td>';
    html += '<td><input type="text" name="qty_'+obj.id+'_'+obj.ivtid+'" value="1" class="span1 num qtyclz"/></td>';
    html += '<td><textarea name="remark_'+obj.id+'_'+obj.ivtid+'"></textarea></td>';
    html += '</tr>';
    $("#pdtlist").append(html);
}

function checkAndAdd(){
    var result = [];
    $("input[name='srhpdt']:checked").each(function(){
        var t = $(this);
        var obj = srhPdtObj(t.val());
        if(obj){ addPdt(obj); }
    })
    $(".num").numeric();
    $('#pdtModal').modal('hide');
}



//*******************************************************
// for the OUT
//*******************************************************

function ajaxSrhPdtByIvt(){
    var params = {
        'type' : 'B',
        'no' : $("#pdtno").val(),
        'name' : $("#pdtname").val(),
        'ivtID' : $("#pdtivtID").val(),
        't' : nowstr()
    }
    
    $.getJSON('/ivtnt/ajaxSrhPdt',params,function(r){
        if(r.code!=0){
            alert(r.msg);
        }else{
            srhResult = r.data;
            var html = '';
            for(var i=0;i<r.data.length;i++){
                var t = r.data[i];
                html += '<tr><td><input type="checkbox" name="srhpdt" value="'+t.pdivtid+'"/></td>';
                html += '<td>'+t.no+'</td>';
                html += '<td>'+t.name+'</td>';
                html += '<td>'+t.qty+'</td>';
                html += '<td>'+t.ivtname+'</td>';
                html += '</tr>';
            }            
            $("#srhbody").html(html);
        }
    })
    
}

function addPdtByIvt(obj){
    var html = '<tr>';
    html += '<td>'+obj.no+'</td>';
    html += '<td>'+obj.name+'<input type="hidden" name="product_'+obj.pdivtid+'" value="'+obj.pdivtid+'"/></td>';
    html += '<td>'+obj.ivtname+'</td>';
    html += '<td>'+obj.qty+'</td>';
    html += '<td><input type="text" name="qty_'+obj.pdivtid+'" value="'+obj.qty+'" class="span1 num qtyclz" maxqty="'+obj.qty+'"/></td>';
    html += '<td><textarea name="remark_'+obj.pdivtid+'"></textarea></td>';
    html += '</tr>';
    $("#pdtlist").append(html);
}


function checkAndAddByIvt(){
    var result = [];
    $("input[name='srhpdt']:checked").each(function(){
        var t = $(this);
        var obj = srhPdtObj(t.val());
        if(obj){ addPdtByIvt(obj); }
    })
    $(".num").numeric();
    $('#pdtModal').modal('hide');
}