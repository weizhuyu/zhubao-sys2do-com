var srhResult = [];

function init(){    
    $(".num").numeric();
}

function srhPdtObj(v){
    for(var i=0;i<srhResult.length;i++){
        var t = srhResult[i];
        //if(t.id == id){ return t ;}
        if(t.no == v){ return t;}
    }
    return null;
}

//###############################################################
//  auto complete for the item input 
//
//###############################################################
$(document).ready(function(){
    
    $('#fieldValue').typeahead({
        source: function (query, process) {
            var params = {
                'f' : $("#field").val(),
                'q' : query,
                'direction' : $("#direction").val(),
                't' : nowstr()
            }        
            return $.getJSON("/ivtnt/ajaxSrhPdt", params, function (r) {
                srhResult = r.data;
                var products = [];
                for(var i=0;i<r.data.length;i++){
                    var t = r.data[i];
                    products.push(t.no + '-' + t.name);
                }         
                return process(products);
            });
        },
        updater:function (item) {
            return item;
        }
    });



    $('#fieldValue').keydown(function(event) {
        var t = $(this);
        if (event.which == 13) {
            addProduct();
            event.preventDefault();
            t.val('');            
        }
    }).blur(function(){
        setTimeout('addProduct()', 200)
    });

})


function addProduct(){
    var v = $('#fieldValue').val();
    if(!v){ return ;}
    var no = v.split("-")[0];
    var obj = srhPdtObj(no);
    
    var t = $("#direction").val();
    if(t == 'IN'){
        if(obj){
            //check whether the no is exist
            if(extPdts.indexOf(obj.no) >= 0 ){ return ; }
            else{ extPdts.push(obj.no);}
            var html = '<tr>';
            html += '<td><a href="#" onclick="rmProduct(this)"><i class="icon-remove"></i></a></td>';
            html += '<td><a href="/pdt/view?id='+obj.id+'" target="_blank" class="pdtlink pdtclass">'+obj.no+'</a></td>';
            html += '<td>'+obj.name+'</td>';
            html += '<td>'+obj.desc+'</td>';
            html += '<td><input type="text" class="span1 num" name="qty_'+obj.id+'_0" value=""/></td>';
            html += '<td><textarea name="remark_'+obj.id+'_0" class="span2"></textarea></td>';
            html += '</tr>';
            $("#pdtlist").append(html);
            init();
        }
    }else{
        if(obj){
            //check whether the no is exist
            if(extPdts.indexOf(obj.no) >= 0 ){ return ; }
            else{ extPdts.push(obj.no);}
            
            var html = '';
            for(var i = 0;i<obj.ivts.length;i++){
                var ivt = obj.ivts[i];
                var ivtid = ivt[0];
                var location = ivt[1];
                var qty = ivt[2];
                var kid = obj.pid + '_' + ivtid;
                html += '<tr>';
                html += '<td><a href="#" onclick="rmProduct(this)"><i class="icon-remove"></i></a></td>';
                html += '<td><a href="/pdt/view?id='+obj.id+'" target="_blank" class="pdtlink pdtclass">'+obj.no+'</a></td>';
                html += '<td>'+obj.name+'</td>';
                html += '<td>'+location+'</td>';
                html += '<td>'+qty+'</td>';        
                html += '<td><input type="text" class="span1 num" name="qty_'+kid+'" value="'+qty+'" maxqty="'+qty+'"/></td>';
                html += '<td><textarea name="remark_'+kid+'" class="span2"></textarea></td>';
                html += '</tr>';
            }
            $("#pdtlist").append(html);
            init();
        }
        
    }
    
}


function rmProduct(obj){
    var t = $(obj);
    var tr = $(t.parents("tr")[0]);
    var k = $('.pdtclass',tr).text();
    
    var same = 0;
    $(".pdtclass").each(function(){
        var tmp = $(this);
        if(tmp.text() == k){ same++; }
    });
    
    if(same == 1){
        var lct = extPdts.indexOf(k);
        if(lct >= 0 ){ extPdts.splice(lct,1); } //remove the key in the ext product   
    }

    tr.remove();
    
}